import { Button, ButtonProps } from "@chakra-ui/button";
import Link from "next/link";
import React, { FC } from "react";

interface LinkButtonProps extends ButtonProps {
  href: string;
}
const LinkButton: FC<LinkButtonProps> = (props) => {
  return (
    <Link href={props.href} passHref={true}>
      <Button {...props}>{props.children}</Button>
    </Link>
  );
};

export default LinkButton;
