import {
  FormControl,
  FormErrorMessage,
  FormLabel,
} from "@chakra-ui/form-control";
import { Input } from "@chakra-ui/input";
import { Textarea } from "@chakra-ui/react";
import { ChakraProps } from "@chakra-ui/system";
import { Field, FieldProps } from "formik";
import _ from "lodash";
import React, { FC } from "react";
import { FormError } from "../../../generated/graphql";

const CustomField: FC<
  {
    label: string;
    name: string;
    type?: "text" | "email" | "password";
    customErrors?: FormError[];
    isTextArea?: boolean;
  } & ChakraProps
> = (props) => {
  return (
    <>
      <Field name={props.name}>
        {({ field, form: { errors } }: FieldProps<any>) => (
          <FormControl
            id={props.name}
            isInvalid={!!errors[props.name] || !!props.customErrors?.length}
            {..._.omit(props, ["customErrors", "isTextArea"])}
          >
            <FormLabel>{props.label}</FormLabel>
            {props.isTextArea ? (
              <Textarea {...field} />
            ) : (
              <Input {...field} type={props.type || "text"} />
            )}

            <FormErrorMessage>
              {errors[props.name]}
              {!errors[props.name] && props.customErrors?.length && (
                <> {props.customErrors[0].message}</>
              )}
            </FormErrorMessage>
          </FormControl>
        )}
      </Field>
    </>
  );
};

export default CustomField;
