import { Center, Container, Text } from "@chakra-ui/layout";
import { FC } from "react";

const Footer: FC = () => {
  return (
    <Container maxW="container.xl" py={2}>
      <Center>
        <Text fontWeight="bold">ondra belza</Text>
      </Center>
    </Container>
  );
};

export default Footer;
