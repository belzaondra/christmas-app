import { useApolloClient } from "@apollo/client";
import { Button } from "@chakra-ui/button";
import { Box, Container, Heading, Text } from "@chakra-ui/layout";
import { Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/react";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { FiChevronDown } from "react-icons/fi";
import { useLogoutMutation, useMeQuery } from "../../../generated/graphql";
import withApollo from "../../../lib/withApollo";
import LinkButton from "../buttons/linkButton";
interface NavbarProps {}

const Navbar: React.FC<NavbarProps> = () => {
  const { data: meData, loading: meLoading } = useMeQuery();
  const router = useRouter();
  const [logoutMutation, { data, loading, error }] = useLogoutMutation();
  const apolloClient = useApolloClient();
  const logout = async () => {
    await logoutMutation();
    router.push("/");

    await apolloClient.cache.reset();
  };

  return (
    <>
      <Container maxW="container.xl" my={2} display="flex" alignItems="center">
        <Box w={["full", "auto"]}>
          <Link href="/" passHref={true}>
            <Heading cursor="pointer" fontSize="xl">
              vánoce {new Date().getFullYear()}
            </Heading>
          </Link>
        </Box>
        {!meLoading && (
          <>
            <Box ml={"auto"} display={["none", "block"]}>
              {!meData?.me ? (
                <>
                  <LinkButton href="/login">přihlásit</LinkButton>
                  <LinkButton href="/registration" ml={2}>
                    registrovat
                  </LinkButton>
                </>
              ) : (
                <>
                  <Text
                    textTransform="lowercase"
                    display="inline-block"
                    color="teal.400"
                  >
                    @{meData.me?.username}
                  </Text>
                  <Button ml={2} onClick={logout}>
                    odhlásit
                  </Button>
                </>
              )}
            </Box>
            <Box display={["block", "none"]}>
              <Menu size="sm">
                <MenuButton
                  as={Button}
                  rightIcon={<FiChevronDown />}
                  textTransform="lowercase"
                >
                  <Box textColor={meData?.me ? "teal.300" : "white"}>
                    {meData?.me ? <> @{meData.me.username} </> : <>menu </>}
                  </Box>
                </MenuButton>
                <MenuList>
                  {meData?.me ? (
                    <>
                      <MenuItem id="menu_logout" onClick={logout}>
                        odhlásit
                      </MenuItem>
                    </>
                  ) : (
                    <>
                      <MenuItem
                        id="menu_login"
                        onClick={() => router.push("/login")}
                      >
                        přihlásit
                      </MenuItem>
                      <MenuItem
                        id="menu_register"
                        onClick={() => router.push("/registration")}
                      >
                        registrovat
                      </MenuItem>
                    </>
                  )}
                </MenuList>
              </Menu>
            </Box>
          </>
        )}
      </Container>
    </>
  );
};

export default withApollo(Navbar);
