import { Box, Flex } from "@chakra-ui/layout";
import { Container } from "@chakra-ui/react";
import React from "react";
import Footer from "./footer";
import Navbar from "./navbar";

interface LayoutProps {}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <>
      <Flex flexDirection="column" minH="100vh">
        <Navbar />
        <Box flexGrow={1}>
          <Container maxW="container.xl">{children}</Container>
        </Box>

        <Footer />
      </Flex>
    </>
  );
};

export default Layout;
