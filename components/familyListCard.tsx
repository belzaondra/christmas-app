import { Box, Flex, Text } from "@chakra-ui/layout";
import React, { FC } from "react";
import { useMeQuery } from "../generated/graphql";
import DeleteFamilyModal from "./modals/delateFamilyModal";
import InviteToFamilyModal from "./modals/inviteToFamilyModal";
import LinkButton from "./shared/buttons/linkButton";
interface IFamilyListCardProps {
  id: string;
  name: string;
  admins: { id: string }[];
  members: { id: string }[];
}
const FamilyListCard: FC<IFamilyListCardProps> = ({
  name,
  id,
  admins,
  members,
}) => {
  const { data, loading } = useMeQuery();
  return (
    <Box p={2} h="full">
      <Flex
        direction="column"
        h="full"
        borderWidth="1px"
        borderRadius="lg"
        p={2}
      >
        <Text
          fontSize="lg"
          fontWeight="bold"
          textAlign="center"
          textTransform="lowercase"
          my={2}
        >
          {name}
        </Text>
        <Text fontWeight="bold" textTransform="lowercase" my={2}>
          počet členů: {members.length}
        </Text>

        <Flex flexGrow={1} />

        <Flex>
          {members.some((member) => member.id === data?.me?.id) && (
            <LinkButton href={`/families/${id}`} flexGrow={1}>
              detail
            </LinkButton>
          )}
          <Flex ml={2} flexShrink={1}>
            <InviteToFamilyModal id={id} />
            {admins.some((admin) => admin.id === data?.me?.id) && (
              <Box ml={2}>
                <DeleteFamilyModal id={id} />
              </Box>
            )}
          </Flex>
        </Flex>
      </Flex>
    </Box>
  );
};
export default FamilyListCard;
