import {
  Alert,
  AlertIcon,
  Button,
  IconButton,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Tooltip,
  useDisclosure,
} from "@chakra-ui/react";
import React, { FC, useState } from "react";
import { Error, useDeleteFamilyMutation } from "../../generated/graphql";
import { AiOutlineDelete } from "react-icons/ai";
import { toast } from "react-toastify";
interface DeleteFamilyModalProps {
  id: string;
}
const DeleteFamilyModal: FC<DeleteFamilyModalProps> = ({ id }) => {
  const [DeleteFamilyMutation, { loading }] = useDeleteFamilyMutation();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [errorMessages, setErrorMessages] = useState<Error[]>();

  const deleteFamily = async () => {
    const response = await DeleteFamilyMutation({
      variables: { id },
      refetchQueries: ["myFamilies"],
    });
    if (response.data?.deleteFamily.errors)
      setErrorMessages(response.data.deleteFamily.errors);
    else {
      onClose();
      toast.success("rodina smazána");
    }
  };
  return (
    <>
      <Tooltip label="smazat rodinu" hasArrow placement="top" bg="red.100">
        <IconButton
          onClick={onOpen}
          colorScheme="red"
          aria-label="smazat rodinu"
          w="full"
          icon={<AiOutlineDelete />}
        />
      </Tooltip>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>opravdu chcete smazat rodinu?</ModalHeader>

          <ModalCloseButton />
          <ModalBody>
            {errorMessages?.map((errorMessage, index) => (
              <Alert
                status="error"
                textTransform="lowercase"
                mt={4}
                key={`${errorMessage}-${index}`}
              >
                <AlertIcon />
                {errorMessage.message}
              </Alert>
            ))}
          </ModalBody>
          <ModalFooter>
            <Button
              mr={3}
              isLoading={loading}
              onClick={deleteFamily}
              colorScheme="red"
            >
              smazat rodinu
            </Button>
            <Button variant="ghost" disabled={loading} onClick={onClose}>
              zavřít
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default DeleteFamilyModal;
