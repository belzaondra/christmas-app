import {
  Button,
  IconButton,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Tooltip,
  useDisclosure,
} from "@chakra-ui/react";
import { Form, Formik } from "formik";
import React, { FC } from "react";
import { BsGiftFill } from "react-icons/bs";
import { toast } from "react-toastify";
import * as Yup from "yup";
import { useCreateWishMutation } from "../../generated/graphql";
import CustomField from "../shared/inputs/CustomFiled";

const CreateGiftSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "příliš krátké!")
    .max(100, "příliš dlouhé!")
    .required("požadováno"),
  description: Yup.string()
    .min(2, "příliš krátké!")
    .max(300, "příliš dlouhé!")
    .required("požadováno"),
  link: Yup.string()
    .min(2, "příliš krátké!")
    .max(300, "příliš dlouhé!")
    .url("neplatný odkaz"),
});

const CreateWishModal: FC<{ id: string; refetch: () => {} }> = ({
  id,
  refetch,
}) => {
  const [createGiftMutation] = useCreateWishMutation();
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Tooltip label="přidat nové přání" hasArrow placement="top">
        <IconButton
          onClick={onOpen}
          icon={<BsGiftFill />}
          aria-label="přidat nový dárek"
          colorScheme="green"
        >
          přidat nové přání
        </IconButton>
      </Tooltip>

      <Formik
        initialValues={{
          name: "",
          description: "",
          link: "",
        }}
        validationSchema={CreateGiftSchema}
        validateOnMount={true}
        onSubmit={async (values, actions) => {
          try {
            const response = await createGiftMutation({
              variables: { ...values, familyId: id },
            });
            refetch();
            actions.resetForm();
            onClose();
            toast.success("přání vytvořeno");
          } catch (error: any) {
            toast.error("nastala nečekaná chyba");
          }
        }}
      >
        {({ isSubmitting, isValidating, isValid, submitForm }) => (
          <Form>
            <Modal isOpen={isOpen} onClose={onClose}>
              <ModalOverlay />
              <ModalContent>
                <ModalHeader>vytvořit nové přání</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <CustomField name="name" label="název" />
                  <CustomField name="description" label="popisek" isTextArea />
                  <CustomField name="link" label="odkaz" />
                </ModalBody>

                <ModalFooter>
                  <Button
                    mr={3}
                    isLoading={isValidating || isSubmitting}
                    disabled={!isValid}
                    onClick={submitForm}
                  >
                    vytvořit
                  </Button>
                  <Button
                    variant="ghost"
                    disabled={isSubmitting}
                    onClick={onClose}
                  >
                    zavřít
                  </Button>
                </ModalFooter>
              </ModalContent>
            </Modal>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default CreateWishModal;
