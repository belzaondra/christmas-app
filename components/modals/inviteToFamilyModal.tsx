import { Button, IconButton } from "@chakra-ui/button";
import { useDisclosure } from "@chakra-ui/hooks";
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/modal";
import { Code, Text, Tooltip } from "@chakra-ui/react";
import React, { FC } from "react";
import { FaCopy } from "react-icons/fa";
import { RiHeartAddFill } from "react-icons/ri";
import copy from "copy-to-clipboard";
import { toast } from "react-toastify";

interface InviteToFamilyModalProps {
  id: string;
}
const InviteToFamilyModal: FC<InviteToFamilyModalProps> = ({ id }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Tooltip label="pozvat nového člena" hasArrow placement="top">
        <IconButton
          aria-label="pozvat nového člena"
          colorScheme="green"
          onClick={onOpen}
          icon={<RiHeartAddFill />}
        />
      </Tooltip>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>pozvání nového člena do rodiny</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Text mb={4} textAlign="center" fontWeight="bold">
              pro pozvání nového člena do rodiny stačí sdílet tento kód s
              uživatelem, kterého chcete pozvat do rodiny.
            </Text>
            <Code p={2} display="block" textAlign="center">
              {id}
            </Code>
            <Tooltip label="zkopírovat do schránky" hasArrow placement="top">
              <IconButton
                aria-label="zkopírovat do schránky"
                icon={<FaCopy />}
                w="full"
                mt={4}
                onClick={() => {
                  copy(id);
                  toast.success("zkopírováno");
                }}
              />
            </Tooltip>
          </ModalBody>

          <ModalFooter>
            <Button variant="ghost" onClick={onClose}>
              zavřít
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};
export default InviteToFamilyModal;
