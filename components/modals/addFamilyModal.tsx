import {
  Alert,
  AlertIcon,
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react";
import { Form, Formik } from "formik";
import React, { FC, useState } from "react";
import { toast } from "react-toastify";
import * as Yup from "yup";
import { FormError, useCreateFamilyMutation } from "../../generated/graphql";
import CustomField from "../shared/inputs/CustomFiled";
const NewFamilySchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "příliš krátké!")
    .max(75, "příliš dlouhé!")
    .required("požadováno"),
});

const AddFamilyModal: FC = () => {
  const [createFamilyMutation] = useCreateFamilyMutation();
  const [errorMessages, setErrorMessages] = useState<FormError[]>();
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Button onClick={onOpen} w="full">
        založit novou rodinu
      </Button>

      <Formik
        initialValues={{
          name: "",
        }}
        validationSchema={NewFamilySchema}
        validateOnMount={true}
        onSubmit={async (values, actions) => {
          try {
            const response = await createFamilyMutation({
              variables: values,
              refetchQueries: ["myFamilies"],
            });
            if (response.data?.createFamily.errors)
              setErrorMessages(response.data.createFamily.errors);
            else {
              actions.resetForm();
              onClose();
              toast.success("rodina založena");
            }
          } catch (error: any) {
            toast.error("nastala nečekaná chyba");
          }
        }}
      >
        {({ isSubmitting, isValidating, isValid, submitForm }) => (
          <Form>
            <Modal isOpen={isOpen} onClose={onClose}>
              <ModalOverlay />
              <ModalContent>
                <ModalHeader>založení nové rodiny</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  {errorMessages
                    ?.filter((errMsg) => !errMsg.field)
                    .map((errorMessage, index) => (
                      <Alert
                        status="error"
                        textTransform="lowercase"
                        mt={4}
                        key={`${errorMessage}-${index}`}
                      >
                        <AlertIcon />
                        {errorMessage.message}
                      </Alert>
                    ))}
                  <CustomField
                    name="name"
                    label="název rodiny"
                    customErrors={errorMessages?.filter(
                      (errMsg) => errMsg.field === "name"
                    )}
                  />
                </ModalBody>

                <ModalFooter>
                  <Button
                    mr={3}
                    isLoading={isValidating || isSubmitting}
                    disabled={!isValid}
                    type="submit"
                    onClick={submitForm}
                  >
                    přidat rodinu
                  </Button>
                  <Button
                    variant="ghost"
                    disabled={isValidating || isSubmitting}
                    onClick={onClose}
                  >
                    zavřít
                  </Button>
                </ModalFooter>
              </ModalContent>
            </Modal>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default AddFamilyModal;
