import {
  Alert,
  AlertIcon,
  Button,
  Center,
  IconButton,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  Tooltip,
  useDisclosure,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import React, { FC, useState } from "react";
import { RiDoorOpenFill } from "react-icons/ri";
import { toast } from "react-toastify";
import {
  Error,
  MyFamiliesDocument,
  MyFamiliesQuery,
  useLeaveFamilyMutation,
} from "../../generated/graphql";
interface LeaveFamilyModalProps {
  id: string;
}
const LeaveFamilyModal: FC<LeaveFamilyModalProps> = ({ id }) => {
  const [LeaveFamilyMutation, { loading }] = useLeaveFamilyMutation();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const router = useRouter();
  const [errorMessages, setErrorMessages] = useState<Error[]>();

  const leaveFamily = async () => {
    const response = await LeaveFamilyMutation({
      variables: { id },
      update: (store) => {
        const myFamilies = store.readQuery<MyFamiliesQuery>({
          query: MyFamiliesDocument,
        });

        if (myFamilies) {
          const filteredFamilies = myFamilies!.myFamilies.filter(
            (f) => f.id !== id
          );
          store.writeQuery<MyFamiliesQuery>({
            query: MyFamiliesDocument,
            data: {
              myFamilies: [...filteredFamilies],
            },
          });
        }
      },
    });
    if (response.data?.leaveFamily.errors)
      setErrorMessages(response.data.leaveFamily.errors);
    else {
      router.push("/families");
      onClose();
      toast.success("rodina opuštěna");
    }
  };
  return (
    <>
      <Tooltip label="opustit rodinu" hasArrow placement="top" bg="red.100">
        <IconButton
          onClick={onOpen}
          colorScheme="red"
          aria-label="opustit rodinu"
          icon={<RiDoorOpenFill />}
        />
      </Tooltip>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>opravdu chcete opustit rodinu?</ModalHeader>

          <ModalCloseButton />
          <ModalBody>
            {errorMessages?.map((errorMessage, index) => (
              <Alert
                status="error"
                textTransform="lowercase"
                mt={4}
                key={`${errorMessage}-${index}`}
              >
                <AlertIcon />
                {errorMessage.message}
              </Alert>
            ))}
            <Center>
              <Text>dojde k odpárování dárku s touto rodinou</Text>
            </Center>
          </ModalBody>
          <ModalFooter>
            <Button
              mr={3}
              isLoading={loading}
              onClick={leaveFamily}
              colorScheme="red"
            >
              opustit rodinu
            </Button>
            <Button variant="ghost" disabled={loading} onClick={onClose}>
              zavřít
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default LeaveFamilyModal;
