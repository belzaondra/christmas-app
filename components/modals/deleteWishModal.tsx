import {
  Button,
  Modal,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react";
import React, { FC } from "react";
import { MdRemoveCircleOutline } from "react-icons/md";
import { toast } from "react-toastify";
import { useDeleteWishMutation } from "../../generated/graphql";
interface DeleteWishModalProps {
  id: string;
  familyId: string;
  refetch: () => {};
}
const DeleteWishModal: FC<DeleteWishModalProps> = ({
  id,
  familyId,
  refetch,
}) => {
  const [LeaveFamilyMutation, { loading }] = useDeleteWishMutation();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const deleteWish = async () => {
    const response = await LeaveFamilyMutation({
      variables: { id, familyId },
    });
    if (!response.errors) {
      refetch();
      onClose();
      toast.success("přání smazáno");
    }
  };
  return (
    <>
      <Button
        textColor="red.300"
        variant="link"
        alignItems="center"
        alignContent="center"
        display="flex"
        onClick={onOpen}
      >
        <MdRemoveCircleOutline size={20} />
        smazat
      </Button>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>opravdu chcete smazat dárek?</ModalHeader>

          <ModalCloseButton />

          <ModalFooter>
            <Button
              mr={3}
              isLoading={loading}
              onClick={deleteWish}
              colorScheme="red"
            >
              smazat dárek
            </Button>
            <Button variant="ghost" disabled={loading} onClick={onClose}>
              zavřít
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default DeleteWishModal;
