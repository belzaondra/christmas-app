import {
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react";
import { Form, Formik } from "formik";
import React, { FC } from "react";
import { MdModeEditOutline } from "react-icons/md";
import { toast } from "react-toastify";
import * as Yup from "yup";
import { useUpdateWishMutation } from "../../generated/graphql";
import CustomField from "../shared/inputs/CustomFiled";

const UpdateWishSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "příliš krátké!")
    .max(100, "příliš dlouhé!")
    .required("požadováno"),
  description: Yup.string()
    .min(2, "příliš krátké!")
    .max(300, "příliš dlouhé!")
    .required("požadováno"),
  link: Yup.string()
    .min(2, "příliš krátké!")
    .max(300, "příliš dlouhé!")
    .url("neplatný odkaz"),
});

const UpdateWishModal: FC<{
  gift: {
    id: string;
    name: string;
    description: string;
    link: string;
  };
  refetch: () => {};
}> = ({ gift, refetch }) => {
  const [updateWishMutation] = useUpdateWishMutation();
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Formik
        initialValues={{
          name: gift.name,
          description: gift.description,
          link: gift.link,
        }}
        validationSchema={UpdateWishSchema}
        validateOnMount={true}
        onSubmit={async (values, actions) => {
          try {
            const response = await updateWishMutation({
              variables: { ...values, id: gift.id },
            });
            refetch();
            onClose();
            actions.resetForm();
            toast.success("přání aktualizováno");
          } catch (error: any) {
            toast.error("nastala nečekaná chyba");
          }
        }}
      >
        {({ isSubmitting, isValidating, isValid, submitForm, resetForm }) => (
          <>
            <Button
              textColor="yellow.300"
              variant="link"
              alignItems="center"
              alignContent="center"
              display="flex"
              onClick={() => {
                resetForm({
                  values: {
                    name: gift.name,
                    description: gift.description,
                    link: gift.link,
                  },
                });
                onOpen();
              }}
            >
              <MdModeEditOutline size={20} />
              aktualizovat
            </Button>
            <Form style={{ display: "contents" }}>
              <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                  <ModalHeader>aktualizovat přání</ModalHeader>
                  <ModalCloseButton />
                  <ModalBody>
                    <CustomField name="name" label="název" />
                    <CustomField
                      name="description"
                      label="popisek"
                      isTextArea
                    />
                    <CustomField name="link" label="odkaz" />
                  </ModalBody>

                  <ModalFooter>
                    <Button
                      mr={3}
                      isLoading={isValidating || isSubmitting}
                      disabled={!isValid}
                      onClick={submitForm}
                    >
                      aktualizovat
                    </Button>
                    <Button
                      variant="ghost"
                      disabled={isSubmitting}
                      onClick={onClose}
                    >
                      zavřít
                    </Button>
                  </ModalFooter>
                </ModalContent>
              </Modal>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
};

export default UpdateWishModal;
