import {
  Alert,
  AlertIcon,
  Button,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
} from "@chakra-ui/react";
import { Form, Formik } from "formik";
import React, { FC, useState } from "react";
import { toast } from "react-toastify";
import * as Yup from "yup";
import { Error, useJoinFamilyMutation } from "../../generated/graphql";
import CustomField from "../shared/inputs/CustomFiled";
const NewFamilySchema = Yup.object().shape({
  id: Yup.string().uuid("neplatný formát!").required("požadováno"),
});

const JoinFamilyModal: FC = () => {
  const [joinFamilyMutation] = useJoinFamilyMutation();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [errorMessages, setErrorMessages] = useState<Error[]>();

  return (
    <>
      <Button onClick={onOpen} w="full">
        přidat se do rodiny
      </Button>
      <Formik
        initialValues={{
          id: "",
        }}
        validationSchema={NewFamilySchema}
        validateOnMount={true}
        onSubmit={async (values, actions) => {
          try {
            const response = await joinFamilyMutation({
              variables: values,
              refetchQueries: ["myFamilies"],
            });

            if (response.data?.joinFamily.errors)
              setErrorMessages(response.data.joinFamily.errors);
            else {
              actions.resetForm();
              onClose();
              toast.success("vítejte v rodině");
            }
          } catch (error: any) {
            toast.error("nastala nečekaná chyba");
          }
        }}
      >
        {({ isSubmitting, isValidating, isValid, submitForm }) => (
          <Form>
            <Modal isOpen={isOpen} onClose={onClose}>
              <ModalOverlay />
              <ModalContent>
                <ModalHeader>připojit se do rodiny</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  {errorMessages?.map((errorMessage, index) => (
                    <Alert
                      status="error"
                      textTransform="lowercase"
                      mt={4}
                      key={`${errorMessage}-${index}`}
                    >
                      <AlertIcon />
                      {errorMessage.message}
                    </Alert>
                  ))}
                  <CustomField name="id" label="identifikátor rodiny" />
                </ModalBody>

                <ModalFooter>
                  <Button
                    mr={3}
                    isLoading={isValidating || isSubmitting}
                    disabled={!isValid}
                    onClick={submitForm}
                  >
                    přidat se
                  </Button>
                  <Button
                    variant="ghost"
                    disabled={isSubmitting}
                    onClick={onClose}
                  >
                    zavřít
                  </Button>
                </ModalFooter>
              </ModalContent>
            </Modal>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default JoinFamilyModal;
