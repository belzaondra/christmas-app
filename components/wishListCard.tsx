import { Box, Flex, Heading, Link, Text } from "@chakra-ui/layout";
import { Button } from "@chakra-ui/react";
import React, { FC } from "react";
import { BsLink } from "react-icons/bs";
import { FaHandsHelping } from "react-icons/fa";
import { GiStopSign } from "react-icons/gi";
import { IoIosDoneAll } from "react-icons/io";
import { toast } from "react-toastify";
import { useChangeWishStatusMutation } from "../generated/graphql";
import DeleteWishModal from "./modals/deleteWishModal";
import UpdateWishModal from "./modals/updateWishModal";
interface WishListCardProps {
  gift: {
    id: string;
    name: string;
    description: string;
    link: string;
    state: string;
    user?: { id: string; username: string } | null;
    giver?: { id: string; username: string } | null;
  };
  user: {
    id: string;
  };
  familyId: string;
  refetch: () => {};
}
const WishListCard: FC<WishListCardProps> = (props) => {
  const { gift, user, refetch, familyId } = props;
  const [changeWishStatusMutation, { loading }] = useChangeWishStatusMutation();

  const getBorderColor = (state: string) => {
    {
      if (state === "NEW" || gift?.user?.id === user.id)
        return "whiteAlpha.300";
      else if (state === "PENDING") return "yellow.300";
      else if (state === "FINISHED") return "green.300";
    }
  };

  const changeState = async (newState: string) => {
    try {
      await changeWishStatusMutation({
        variables: {
          id: gift.id,
          state: newState,
        },
      });

      refetch();
      toast.success("status aktualizován");
    } catch (error) {
      toast.error("nastala nečekaná chyba");
    }
  };
  return (
    <Box p={2} h="full">
      <Flex
        borderWidth="1px"
        borderRadius="lg"
        borderColor={getBorderColor(gift.state)}
        display="flex"
        flexDirection="column"
        p={2}
        h="full">
        <Heading
          as="h6"
          fontSize="xl"
          textAlign="center"
          textTransform="lowercase">
          {gift.name}
        </Heading>
        <Text textAlign="center" my={2} textTransform="lowercase">
          {gift.description}
        </Text>
        <Flex flexGrow={1} />
        {gift.giver && gift?.user?.id !== user.id && (
          <Text
            textAlign="center"
            textTransform="lowercase"
            fontStyle="italic"
            mb={2}>
            {gift.state === "PENDING" ? "zařizuje" : "zařídil/a"}{" "}
            {gift.giver.username}
          </Text>
        )}

        {!loading && (
          <Flex justifyContent="space-evenly">
            {gift.giver?.id === user.id && (
              <Button
                variant="link"
                textColor="red.300"
                size="sm"
                onClick={() => changeState("NEW")}
                isLoading={loading}>
                <Box mr={1}>
                  <GiStopSign size={25} />
                </Box>
                nezařízeno
              </Button>
            )}

            {gift.state === "NEW" && gift?.user?.id !== user.id && (
              <Button
                variant="link"
                textColor="yellow.300"
                size="sm"
                onClick={() => changeState("PENDING")}
                isLoading={loading}>
                <FaHandsHelping size={25} />
                zařídím
              </Button>
            )}

            {(gift?.user?.id !== user.id &&
              gift.state === "PENDING" &&
              gift.giver?.id === user.id) ||
            (gift.state === "NEW" && gift?.user?.id !== user.id) ? (
              <>
                <Button
                  variant="link"
                  textColor="green.300"
                  size="sm"
                  onClick={() => changeState("FINISHED")}
                  isLoading={loading}>
                  <IoIosDoneAll size={35} />
                  zařízeno
                </Button>
              </>
            ) : null}

            {gift.link && (
              <Link href={gift.link} target="_blank" display="contents">
                <Button variant="link" textColor="teal.300" size="sm">
                  <BsLink size={25} />
                  odkaz
                </Button>
              </Link>
            )}

            {gift?.user?.id === user.id && (
              <>
                <DeleteWishModal
                  familyId={familyId}
                  id={gift.id}
                  refetch={refetch}
                />
                <UpdateWishModal refetch={refetch} gift={gift} />
              </>
            )}
          </Flex>
        )}
      </Flex>
    </Box>
  );
};

export default WishListCard;
