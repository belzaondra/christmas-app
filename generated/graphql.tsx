import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type ChangeWishStateInput = {
  id: Scalars['String'];
  state: Scalars['String'];
};

export type CreateFamilyResponse = {
  __typename?: 'CreateFamilyResponse';
  errors?: Maybe<Array<FormError>>;
  family?: Maybe<Family>;
};

export type CreateWishInput = {
  description: Scalars['String'];
  familyId: Scalars['String'];
  link: Scalars['String'];
  name: Scalars['String'];
};

export type Error = {
  __typename?: 'Error';
  message: Scalars['String'];
};

export type Family = {
  __typename?: 'Family';
  admins: Array<User>;
  id: Scalars['String'];
  members: Array<User>;
  name: Scalars['String'];
  wishes: Array<Wish>;
};

export type FamilyActionResponse = {
  __typename?: 'FamilyActionResponse';
  errors?: Maybe<Array<Error>>;
  result: Scalars['Boolean'];
};

export type FormError = {
  __typename?: 'FormError';
  field?: Maybe<Scalars['String']>;
  message: Scalars['String'];
};

export type LoginInput = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  changeWishState: Scalars['Boolean'];
  createFamily: CreateFamilyResponse;
  createWish: Wish;
  deleteFamily: FamilyActionResponse;
  deleteWish: Scalars['Boolean'];
  joinFamily: FamilyActionResponse;
  leaveFamily: FamilyActionResponse;
  login: UserFormResponse;
  logout: Scalars['Boolean'];
  register: UserFormResponse;
  updateWish: Scalars['Boolean'];
};


export type MutationChangeWishStateArgs = {
  model: ChangeWishStateInput;
};


export type MutationCreateFamilyArgs = {
  name: Scalars['String'];
};


export type MutationCreateWishArgs = {
  model: CreateWishInput;
};


export type MutationDeleteFamilyArgs = {
  id: Scalars['String'];
};


export type MutationDeleteWishArgs = {
  familyId: Scalars['String'];
  id: Scalars['String'];
};


export type MutationJoinFamilyArgs = {
  id: Scalars['String'];
};


export type MutationLeaveFamilyArgs = {
  id: Scalars['String'];
};


export type MutationLoginArgs = {
  loginInput: LoginInput;
};


export type MutationRegisterArgs = {
  RegistrationInput: RegistrationInput;
};


export type MutationUpdateWishArgs = {
  model: UpdateWishInput;
};

export type Query = {
  __typename?: 'Query';
  families: Array<Family>;
  family: Family;
  me?: Maybe<User>;
  myFamilies: Array<Family>;
};


export type QueryFamilyArgs = {
  id: Scalars['String'];
};

export type RegistrationInput = {
  email: Scalars['String'];
  password: Scalars['String'];
  username: Scalars['String'];
};

export type UpdateWishInput = {
  description: Scalars['String'];
  id: Scalars['String'];
  link: Scalars['String'];
  name: Scalars['String'];
};

export type User = {
  __typename?: 'User';
  adminOfFamilies: Array<Family>;
  email: Scalars['String'];
  families: Array<Family>;
  gifts: Array<Wish>;
  id: Scalars['String'];
  role: Scalars['String'];
  username: Scalars['String'];
  wishes: Array<Wish>;
};

export type UserFormResponse = {
  __typename?: 'UserFormResponse';
  errors?: Maybe<Array<FormError>>;
  user?: Maybe<User>;
};

export type Wish = {
  __typename?: 'Wish';
  description: Scalars['String'];
  families: Array<Family>;
  giver?: Maybe<User>;
  id: Scalars['String'];
  link: Scalars['String'];
  name: Scalars['String'];
  state: Scalars['String'];
  user: User;
};

export type CreateFamilyMutationVariables = Exact<{
  name: Scalars['String'];
}>;


export type CreateFamilyMutation = { __typename?: 'Mutation', createFamily: { __typename?: 'CreateFamilyResponse', family?: { __typename?: 'Family', id: string, name: string } | null | undefined, errors?: Array<{ __typename?: 'FormError', field?: string | null | undefined, message: string }> | null | undefined } };

export type DeleteFamilyMutationVariables = Exact<{
  id: Scalars['String'];
}>;


export type DeleteFamilyMutation = { __typename?: 'Mutation', deleteFamily: { __typename?: 'FamilyActionResponse', result: boolean, errors?: Array<{ __typename?: 'Error', message: string }> | null | undefined } };

export type JoinFamilyMutationVariables = Exact<{
  id: Scalars['String'];
}>;


export type JoinFamilyMutation = { __typename?: 'Mutation', joinFamily: { __typename?: 'FamilyActionResponse', result: boolean, errors?: Array<{ __typename?: 'Error', message: string }> | null | undefined } };

export type LeaveFamilyMutationVariables = Exact<{
  id: Scalars['String'];
}>;


export type LeaveFamilyMutation = { __typename?: 'Mutation', leaveFamily: { __typename?: 'FamilyActionResponse', result: boolean, errors?: Array<{ __typename?: 'Error', message: string }> | null | undefined } };

export type FamiliesQueryVariables = Exact<{ [key: string]: never; }>;


export type FamiliesQuery = { __typename?: 'Query', families: Array<{ __typename?: 'Family', id: string, name: string, admins: Array<{ __typename?: 'User', id: string }>, members: Array<{ __typename?: 'User', id: string }> }> };

export type FamilyQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type FamilyQuery = { __typename?: 'Query', family: { __typename?: 'Family', id: string, name: string, members: Array<{ __typename?: 'User', id: string, username: string }>, admins: Array<{ __typename?: 'User', id: string, username: string }>, wishes: Array<{ __typename?: 'Wish', id: string, name: string, description: string, link: string, state: string, user: { __typename?: 'User', id: string, username: string }, giver?: { __typename?: 'User', id: string, username: string } | null | undefined }> } };

export type MyFamiliesQueryVariables = Exact<{ [key: string]: never; }>;


export type MyFamiliesQuery = { __typename?: 'Query', myFamilies: Array<{ __typename?: 'Family', id: string, name: string, admins: Array<{ __typename?: 'User', id: string }>, members: Array<{ __typename?: 'User', id: string }> }> };

export type LoginMutationVariables = Exact<{
  username: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = { __typename?: 'Mutation', login: { __typename?: 'UserFormResponse', user?: { __typename?: 'User', id: string, username: string, email: string } | null | undefined, errors?: Array<{ __typename?: 'FormError', field?: string | null | undefined, message: string }> | null | undefined } };

export type LogoutMutationVariables = Exact<{ [key: string]: never; }>;


export type LogoutMutation = { __typename?: 'Mutation', logout: boolean };

export type RegisterMutationVariables = Exact<{
  username: Scalars['String'];
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type RegisterMutation = { __typename?: 'Mutation', register: { __typename?: 'UserFormResponse', user?: { __typename?: 'User', id: string, username: string, email: string } | null | undefined, errors?: Array<{ __typename?: 'FormError', field?: string | null | undefined, message: string }> | null | undefined } };

export type MeQueryVariables = Exact<{ [key: string]: never; }>;


export type MeQuery = { __typename?: 'Query', me?: { __typename?: 'User', id: string, username: string, email: string } | null | undefined };

export type ChangeWishStatusMutationVariables = Exact<{
  id: Scalars['String'];
  state: Scalars['String'];
}>;


export type ChangeWishStatusMutation = { __typename?: 'Mutation', changeWishState: boolean };

export type CreateWishMutationVariables = Exact<{
  name: Scalars['String'];
  description: Scalars['String'];
  link: Scalars['String'];
  familyId: Scalars['String'];
}>;


export type CreateWishMutation = { __typename?: 'Mutation', createWish: { __typename?: 'Wish', id: string, name: string, description: string, link: string } };

export type DeleteWishMutationVariables = Exact<{
  id: Scalars['String'];
  familyId: Scalars['String'];
}>;


export type DeleteWishMutation = { __typename?: 'Mutation', deleteWish: boolean };

export type UpdateWishMutationVariables = Exact<{
  name: Scalars['String'];
  description: Scalars['String'];
  link: Scalars['String'];
  id: Scalars['String'];
}>;


export type UpdateWishMutation = { __typename?: 'Mutation', updateWish: boolean };


export const CreateFamilyDocument = gql`
    mutation createFamily($name: String!) {
  createFamily(name: $name) {
    family {
      id
      name
    }
    errors {
      field
      message
    }
  }
}
    `;
export type CreateFamilyMutationFn = Apollo.MutationFunction<CreateFamilyMutation, CreateFamilyMutationVariables>;

/**
 * __useCreateFamilyMutation__
 *
 * To run a mutation, you first call `useCreateFamilyMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateFamilyMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createFamilyMutation, { data, loading, error }] = useCreateFamilyMutation({
 *   variables: {
 *      name: // value for 'name'
 *   },
 * });
 */
export function useCreateFamilyMutation(baseOptions?: Apollo.MutationHookOptions<CreateFamilyMutation, CreateFamilyMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateFamilyMutation, CreateFamilyMutationVariables>(CreateFamilyDocument, options);
      }
export type CreateFamilyMutationHookResult = ReturnType<typeof useCreateFamilyMutation>;
export type CreateFamilyMutationResult = Apollo.MutationResult<CreateFamilyMutation>;
export type CreateFamilyMutationOptions = Apollo.BaseMutationOptions<CreateFamilyMutation, CreateFamilyMutationVariables>;
export const DeleteFamilyDocument = gql`
    mutation deleteFamily($id: String!) {
  deleteFamily(id: $id) {
    result
    errors {
      message
    }
  }
}
    `;
export type DeleteFamilyMutationFn = Apollo.MutationFunction<DeleteFamilyMutation, DeleteFamilyMutationVariables>;

/**
 * __useDeleteFamilyMutation__
 *
 * To run a mutation, you first call `useDeleteFamilyMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteFamilyMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteFamilyMutation, { data, loading, error }] = useDeleteFamilyMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteFamilyMutation(baseOptions?: Apollo.MutationHookOptions<DeleteFamilyMutation, DeleteFamilyMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteFamilyMutation, DeleteFamilyMutationVariables>(DeleteFamilyDocument, options);
      }
export type DeleteFamilyMutationHookResult = ReturnType<typeof useDeleteFamilyMutation>;
export type DeleteFamilyMutationResult = Apollo.MutationResult<DeleteFamilyMutation>;
export type DeleteFamilyMutationOptions = Apollo.BaseMutationOptions<DeleteFamilyMutation, DeleteFamilyMutationVariables>;
export const JoinFamilyDocument = gql`
    mutation joinFamily($id: String!) {
  joinFamily(id: $id) {
    result
    errors {
      message
    }
  }
}
    `;
export type JoinFamilyMutationFn = Apollo.MutationFunction<JoinFamilyMutation, JoinFamilyMutationVariables>;

/**
 * __useJoinFamilyMutation__
 *
 * To run a mutation, you first call `useJoinFamilyMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useJoinFamilyMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [joinFamilyMutation, { data, loading, error }] = useJoinFamilyMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useJoinFamilyMutation(baseOptions?: Apollo.MutationHookOptions<JoinFamilyMutation, JoinFamilyMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<JoinFamilyMutation, JoinFamilyMutationVariables>(JoinFamilyDocument, options);
      }
export type JoinFamilyMutationHookResult = ReturnType<typeof useJoinFamilyMutation>;
export type JoinFamilyMutationResult = Apollo.MutationResult<JoinFamilyMutation>;
export type JoinFamilyMutationOptions = Apollo.BaseMutationOptions<JoinFamilyMutation, JoinFamilyMutationVariables>;
export const LeaveFamilyDocument = gql`
    mutation LeaveFamily($id: String!) {
  leaveFamily(id: $id) {
    result
    errors {
      message
    }
  }
}
    `;
export type LeaveFamilyMutationFn = Apollo.MutationFunction<LeaveFamilyMutation, LeaveFamilyMutationVariables>;

/**
 * __useLeaveFamilyMutation__
 *
 * To run a mutation, you first call `useLeaveFamilyMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLeaveFamilyMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [leaveFamilyMutation, { data, loading, error }] = useLeaveFamilyMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useLeaveFamilyMutation(baseOptions?: Apollo.MutationHookOptions<LeaveFamilyMutation, LeaveFamilyMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LeaveFamilyMutation, LeaveFamilyMutationVariables>(LeaveFamilyDocument, options);
      }
export type LeaveFamilyMutationHookResult = ReturnType<typeof useLeaveFamilyMutation>;
export type LeaveFamilyMutationResult = Apollo.MutationResult<LeaveFamilyMutation>;
export type LeaveFamilyMutationOptions = Apollo.BaseMutationOptions<LeaveFamilyMutation, LeaveFamilyMutationVariables>;
export const FamiliesDocument = gql`
    query families {
  families {
    id
    name
    admins {
      id
    }
    members {
      id
    }
  }
}
    `;

/**
 * __useFamiliesQuery__
 *
 * To run a query within a React component, call `useFamiliesQuery` and pass it any options that fit your needs.
 * When your component renders, `useFamiliesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFamiliesQuery({
 *   variables: {
 *   },
 * });
 */
export function useFamiliesQuery(baseOptions?: Apollo.QueryHookOptions<FamiliesQuery, FamiliesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FamiliesQuery, FamiliesQueryVariables>(FamiliesDocument, options);
      }
export function useFamiliesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FamiliesQuery, FamiliesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FamiliesQuery, FamiliesQueryVariables>(FamiliesDocument, options);
        }
export type FamiliesQueryHookResult = ReturnType<typeof useFamiliesQuery>;
export type FamiliesLazyQueryHookResult = ReturnType<typeof useFamiliesLazyQuery>;
export type FamiliesQueryResult = Apollo.QueryResult<FamiliesQuery, FamiliesQueryVariables>;
export const FamilyDocument = gql`
    query family($id: String!) {
  family(id: $id) {
    id
    name
    members {
      id
      username
    }
    admins {
      id
      username
    }
    wishes {
      id
      name
      description
      link
      state
      user {
        id
        username
      }
      giver {
        id
        username
      }
    }
  }
}
    `;

/**
 * __useFamilyQuery__
 *
 * To run a query within a React component, call `useFamilyQuery` and pass it any options that fit your needs.
 * When your component renders, `useFamilyQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFamilyQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useFamilyQuery(baseOptions: Apollo.QueryHookOptions<FamilyQuery, FamilyQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<FamilyQuery, FamilyQueryVariables>(FamilyDocument, options);
      }
export function useFamilyLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<FamilyQuery, FamilyQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<FamilyQuery, FamilyQueryVariables>(FamilyDocument, options);
        }
export type FamilyQueryHookResult = ReturnType<typeof useFamilyQuery>;
export type FamilyLazyQueryHookResult = ReturnType<typeof useFamilyLazyQuery>;
export type FamilyQueryResult = Apollo.QueryResult<FamilyQuery, FamilyQueryVariables>;
export const MyFamiliesDocument = gql`
    query myFamilies {
  myFamilies {
    id
    name
    admins {
      id
    }
    members {
      id
    }
  }
}
    `;

/**
 * __useMyFamiliesQuery__
 *
 * To run a query within a React component, call `useMyFamiliesQuery` and pass it any options that fit your needs.
 * When your component renders, `useMyFamiliesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMyFamiliesQuery({
 *   variables: {
 *   },
 * });
 */
export function useMyFamiliesQuery(baseOptions?: Apollo.QueryHookOptions<MyFamiliesQuery, MyFamiliesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MyFamiliesQuery, MyFamiliesQueryVariables>(MyFamiliesDocument, options);
      }
export function useMyFamiliesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MyFamiliesQuery, MyFamiliesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MyFamiliesQuery, MyFamiliesQueryVariables>(MyFamiliesDocument, options);
        }
export type MyFamiliesQueryHookResult = ReturnType<typeof useMyFamiliesQuery>;
export type MyFamiliesLazyQueryHookResult = ReturnType<typeof useMyFamiliesLazyQuery>;
export type MyFamiliesQueryResult = Apollo.QueryResult<MyFamiliesQuery, MyFamiliesQueryVariables>;
export const LoginDocument = gql`
    mutation login($username: String!, $password: String!) {
  login(loginInput: {username: $username, password: $password}) {
    user {
      id
      username
      email
    }
    errors {
      field
      message
    }
  }
}
    `;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      username: // value for 'username'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const LogoutDocument = gql`
    mutation logout {
  logout
}
    `;
export type LogoutMutationFn = Apollo.MutationFunction<LogoutMutation, LogoutMutationVariables>;

/**
 * __useLogoutMutation__
 *
 * To run a mutation, you first call `useLogoutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLogoutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [logoutMutation, { data, loading, error }] = useLogoutMutation({
 *   variables: {
 *   },
 * });
 */
export function useLogoutMutation(baseOptions?: Apollo.MutationHookOptions<LogoutMutation, LogoutMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LogoutMutation, LogoutMutationVariables>(LogoutDocument, options);
      }
export type LogoutMutationHookResult = ReturnType<typeof useLogoutMutation>;
export type LogoutMutationResult = Apollo.MutationResult<LogoutMutation>;
export type LogoutMutationOptions = Apollo.BaseMutationOptions<LogoutMutation, LogoutMutationVariables>;
export const RegisterDocument = gql`
    mutation register($username: String!, $email: String!, $password: String!) {
  register(
    RegistrationInput: {username: $username, email: $email, password: $password}
  ) {
    user {
      id
      username
      email
    }
    errors {
      field
      message
    }
  }
}
    `;
export type RegisterMutationFn = Apollo.MutationFunction<RegisterMutation, RegisterMutationVariables>;

/**
 * __useRegisterMutation__
 *
 * To run a mutation, you first call `useRegisterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerMutation, { data, loading, error }] = useRegisterMutation({
 *   variables: {
 *      username: // value for 'username'
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useRegisterMutation(baseOptions?: Apollo.MutationHookOptions<RegisterMutation, RegisterMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterMutation, RegisterMutationVariables>(RegisterDocument, options);
      }
export type RegisterMutationHookResult = ReturnType<typeof useRegisterMutation>;
export type RegisterMutationResult = Apollo.MutationResult<RegisterMutation>;
export type RegisterMutationOptions = Apollo.BaseMutationOptions<RegisterMutation, RegisterMutationVariables>;
export const MeDocument = gql`
    query me {
  me {
    id
    username
    email
  }
}
    `;

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: Apollo.QueryHookOptions<MeQuery, MeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MeQuery, MeQueryVariables>(MeDocument, options);
      }
export function useMeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MeQuery, MeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, options);
        }
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = Apollo.QueryResult<MeQuery, MeQueryVariables>;
export const ChangeWishStatusDocument = gql`
    mutation ChangeWishStatus($id: String!, $state: String!) {
  changeWishState(model: {id: $id, state: $state})
}
    `;
export type ChangeWishStatusMutationFn = Apollo.MutationFunction<ChangeWishStatusMutation, ChangeWishStatusMutationVariables>;

/**
 * __useChangeWishStatusMutation__
 *
 * To run a mutation, you first call `useChangeWishStatusMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangeWishStatusMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changeWishStatusMutation, { data, loading, error }] = useChangeWishStatusMutation({
 *   variables: {
 *      id: // value for 'id'
 *      state: // value for 'state'
 *   },
 * });
 */
export function useChangeWishStatusMutation(baseOptions?: Apollo.MutationHookOptions<ChangeWishStatusMutation, ChangeWishStatusMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangeWishStatusMutation, ChangeWishStatusMutationVariables>(ChangeWishStatusDocument, options);
      }
export type ChangeWishStatusMutationHookResult = ReturnType<typeof useChangeWishStatusMutation>;
export type ChangeWishStatusMutationResult = Apollo.MutationResult<ChangeWishStatusMutation>;
export type ChangeWishStatusMutationOptions = Apollo.BaseMutationOptions<ChangeWishStatusMutation, ChangeWishStatusMutationVariables>;
export const CreateWishDocument = gql`
    mutation CreateWish($name: String!, $description: String!, $link: String!, $familyId: String!) {
  createWish(
    model: {name: $name, description: $description, link: $link, familyId: $familyId}
  ) {
    id
    name
    description
    link
  }
}
    `;
export type CreateWishMutationFn = Apollo.MutationFunction<CreateWishMutation, CreateWishMutationVariables>;

/**
 * __useCreateWishMutation__
 *
 * To run a mutation, you first call `useCreateWishMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateWishMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createWishMutation, { data, loading, error }] = useCreateWishMutation({
 *   variables: {
 *      name: // value for 'name'
 *      description: // value for 'description'
 *      link: // value for 'link'
 *      familyId: // value for 'familyId'
 *   },
 * });
 */
export function useCreateWishMutation(baseOptions?: Apollo.MutationHookOptions<CreateWishMutation, CreateWishMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateWishMutation, CreateWishMutationVariables>(CreateWishDocument, options);
      }
export type CreateWishMutationHookResult = ReturnType<typeof useCreateWishMutation>;
export type CreateWishMutationResult = Apollo.MutationResult<CreateWishMutation>;
export type CreateWishMutationOptions = Apollo.BaseMutationOptions<CreateWishMutation, CreateWishMutationVariables>;
export const DeleteWishDocument = gql`
    mutation DeleteWish($id: String!, $familyId: String!) {
  deleteWish(id: $id, familyId: $familyId)
}
    `;
export type DeleteWishMutationFn = Apollo.MutationFunction<DeleteWishMutation, DeleteWishMutationVariables>;

/**
 * __useDeleteWishMutation__
 *
 * To run a mutation, you first call `useDeleteWishMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteWishMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteWishMutation, { data, loading, error }] = useDeleteWishMutation({
 *   variables: {
 *      id: // value for 'id'
 *      familyId: // value for 'familyId'
 *   },
 * });
 */
export function useDeleteWishMutation(baseOptions?: Apollo.MutationHookOptions<DeleteWishMutation, DeleteWishMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteWishMutation, DeleteWishMutationVariables>(DeleteWishDocument, options);
      }
export type DeleteWishMutationHookResult = ReturnType<typeof useDeleteWishMutation>;
export type DeleteWishMutationResult = Apollo.MutationResult<DeleteWishMutation>;
export type DeleteWishMutationOptions = Apollo.BaseMutationOptions<DeleteWishMutation, DeleteWishMutationVariables>;
export const UpdateWishDocument = gql`
    mutation UpdateWish($name: String!, $description: String!, $link: String!, $id: String!) {
  updateWish(
    model: {name: $name, description: $description, link: $link, id: $id}
  )
}
    `;
export type UpdateWishMutationFn = Apollo.MutationFunction<UpdateWishMutation, UpdateWishMutationVariables>;

/**
 * __useUpdateWishMutation__
 *
 * To run a mutation, you first call `useUpdateWishMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateWishMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateWishMutation, { data, loading, error }] = useUpdateWishMutation({
 *   variables: {
 *      name: // value for 'name'
 *      description: // value for 'description'
 *      link: // value for 'link'
 *      id: // value for 'id'
 *   },
 * });
 */
export function useUpdateWishMutation(baseOptions?: Apollo.MutationHookOptions<UpdateWishMutation, UpdateWishMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateWishMutation, UpdateWishMutationVariables>(UpdateWishDocument, options);
      }
export type UpdateWishMutationHookResult = ReturnType<typeof useUpdateWishMutation>;
export type UpdateWishMutationResult = Apollo.MutationResult<UpdateWishMutation>;
export type UpdateWishMutationOptions = Apollo.BaseMutationOptions<UpdateWishMutation, UpdateWishMutationVariables>;