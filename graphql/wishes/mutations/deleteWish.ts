import gql from "graphql-tag";

export const deleteWish = gql`
  mutation DeleteWish($id: String!, $familyId: String!) {
    deleteWish(id: $id, familyId: $familyId)
  }
`;
