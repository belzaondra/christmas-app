import gql from "graphql-tag";

export const createNewWish = gql`
  mutation CreateWish(
    $name: String!
    $description: String!
    $link: String!
    $familyId: String!
  ) {
    createWish(
      model: {
        name: $name
        description: $description
        link: $link
        familyId: $familyId
      }
    ) {
      id
      name
      description
      link
    }
  }
`;
