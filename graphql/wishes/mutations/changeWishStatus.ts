import gql from "graphql-tag";

export const changeWishStatus = gql`
  mutation ChangeWishStatus($id: String!, $state: String!) {
    changeWishState(model: { id: $id, state: $state })
  }
`;
