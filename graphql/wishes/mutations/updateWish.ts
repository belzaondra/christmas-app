import gql from "graphql-tag";

export const updateWish = gql`
  mutation UpdateWish(
    $name: String!
    $description: String!
    $link: String!
    $id: String!
  ) {
    updateWish(
      model: { name: $name, description: $description, link: $link, id: $id }
    )
  }
`;
