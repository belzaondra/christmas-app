import gql from "graphql-tag";

export const loginMutation = gql`
  mutation logout {
    logout
  }
`;
