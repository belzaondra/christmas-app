import gql from "graphql-tag";

export const loginMutation = gql`
  mutation register($username: String!, $email: String!, $password: String!) {
    register(
      RegistrationInput: {
        username: $username
        email: $email
        password: $password
      }
    ) {
      user {
        id
        username
        email
      }
      errors {
        field
        message
      }
    }
  }
`;
