import gql from "graphql-tag";

export const loginMutation = gql`
  mutation login($username: String!, $password: String!) {
    login(loginInput: { username: $username, password: $password }) {
      user {
        id
        username
        email
      }
      errors {
        field
        message
      }
    }
  }
`;
