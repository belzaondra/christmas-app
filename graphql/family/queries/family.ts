import gql from "graphql-tag";

export const familyQuery = gql`
  query family($id: String!) {
    family(id: $id) {
      id
      name
      members {
        id
        username
      }
      admins {
        id
        username
      }
      wishes {
        id
        name
        description
        link
        state
        user {
          id
          username
        }
        giver {
          id
          username
        }
      }
    }
  }
`;
