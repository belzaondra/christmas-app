import gql from "graphql-tag";

export const myFamiliesQuery = gql`
  query myFamilies {
    myFamilies {
      id
      name
      admins {
        id
      }
      members {
        id
      }
    }
  }
`;
