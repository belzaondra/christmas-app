import gql from "graphql-tag";

export const familiesQuery = gql`
  query families {
    families {
      id
      name
      admins {
        id
      }
      members {
        id
      }
    }
  }
`;
