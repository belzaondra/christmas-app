import gql from "graphql-tag";

export const createFamilyMutation = gql`
  mutation createFamily($name: String!) {
    createFamily(name: $name) {
      family {
        id
        name
      }
      errors {
        field
        message
      }
    }
  }
`;
