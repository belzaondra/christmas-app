import gql from "graphql-tag";

export const deleteFamilyMutation = gql`
  mutation deleteFamily($id: String!) {
    deleteFamily(id: $id) {
      result
      errors {
        message
      }
    }
  }
`;
