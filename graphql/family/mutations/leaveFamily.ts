import gql from "graphql-tag";

export const leaveFamilyMutation = gql`
  mutation LeaveFamily($id: String!) {
    leaveFamily(id: $id) {
      result
      errors {
        message
      }
    }
  }
`;
