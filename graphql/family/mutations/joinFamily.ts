import gql from "graphql-tag";

export const joinFamilyMutation = gql`
  mutation joinFamily($id: String!) {
    joinFamily(id: $id) {
      result
      errors {
        message
      }
    }
  }
`;
