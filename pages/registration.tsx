import {
  Alert,
  AlertIcon,
  Box,
  Button,
  Center,
  Flex,
  Heading,
} from "@chakra-ui/react";
import { Form, Formik } from "formik";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { toast } from "react-toastify";
import * as Yup from "yup";
import LinkButton from "../components/shared/buttons/linkButton";
import CustomField from "../components/shared/inputs/CustomFiled";
import { FormError, useRegisterMutation } from "../generated/graphql";
import withApollo from "../lib/withApollo";

const LoginSchema = Yup.object().shape({
  username: Yup.string()
    .min(2, "příliš krátké!")
    .max(50, "příliš dlouhé!")
    .required("požadováno"),
  email: Yup.string()
    .email("nevalidní email")
    .min(2, "příliš krátké!")
    .max(255, "příliš dlouhé!")
    .required("požadováno"),
  password: Yup.string()
    .min(2, "příliš krátké!")
    .max(255, "příliš dlouhé!")
    .required("požadováno"),
});

const Registration: NextPage = () => {
  const router = useRouter();
  const [registerMutation] = useRegisterMutation();
  const [errorMessages, setErrorMessages] = useState<FormError[]>();

  return (
    <Center display="flex" flexDirection="column">
      <Box w="full" maxW="md">
        <Heading as="h3">registrace</Heading>
        <Formik
          initialValues={{
            username: "",
            email: "",
            password: "",
          }}
          validationSchema={LoginSchema}
          validateOnMount={true}
          onSubmit={async (values) => {
            try {
              const response = await registerMutation({
                variables: values,
                refetchQueries: ["me"],
              });

              if (response.data?.register.user) router.push("/");
              else if (response.data?.register.errors) {
                const { errors } = response.data.register;
                setErrorMessages(errors);
              }
            } catch (error: any) {
              toast.error("nastala nečekaná chyba");
            }
          }}
        >
          {({ isValid, isValidating, isSubmitting }) => (
            <Form>
              {errorMessages
                ?.filter((errMsg) => !errMsg.field)
                .map((errorMessage, index) => (
                  <Alert
                    status="error"
                    textTransform="lowercase"
                    mt={4}
                    key={`${errorMessage}-${index}`}
                  >
                    <AlertIcon />
                    {errorMessage.message}
                  </Alert>
                ))}
              <CustomField
                name="username"
                label="uživatelské jméno"
                mt={4}
                customErrors={errorMessages?.filter(
                  (errMsg) => errMsg.field === "username"
                )}
              />
              <CustomField
                name="email"
                type="email"
                label="email"
                customErrors={errorMessages?.filter(
                  (errMsg) => errMsg.field === "email"
                )}
                mt={4}
              />
              <CustomField
                name="password"
                type="password"
                label="heslo"
                customErrors={errorMessages?.filter(
                  (errMsg) => errMsg.field === "password"
                )}
                mt={4}
              />
              <Flex justifyContent="center" my={4}>
                <Button
                  mr={2}
                  type="submit"
                  disabled={!isValid}
                  isLoading={isSubmitting || isValidating}
                >
                  registrovat
                </Button>
                <LinkButton ml={2} href="/login">
                  již mám účet
                </LinkButton>
              </Flex>
            </Form>
          )}
        </Formik>
        {/* 
        <FormControl id="username" mt={4}>
          <FormLabel>uživatelské jméno</FormLabel>
          <Input type="text" name="username" onChange={handleFormChange} />
        </FormControl>

        <FormControl id="email" mt={4}>
          <FormLabel>email</FormLabel>
          <Input type="email" name="email" onChange={handleFormChange} />
        </FormControl>

        <FormControl id="password" mt={4}>
          <FormLabel>heslo</FormLabel>
          <Input type="password" name="password" onChange={handleFormChange} />
        </FormControl> */}
      </Box>
    </Center>
  );
};

export default withApollo(Registration);
