import type { AppProps } from "next/app";
import React from "react";
import Layout from "../components/shared/layout/layout";
import { ChakraProvider } from "@chakra-ui/react";
import "../styles/globals.css";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import Head from "next/head";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider>
      <Head>
        <title>Christmas app</title>
        <meta name="description" content="Stránka pro správu vánočních přání" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout>
        <ToastContainer
          position="bottom-right"
          autoClose={5000}
          closeOnClick
          pauseOnHover
          draggable
          theme="dark"
        />
        <Component {...pageProps} />
      </Layout>
    </ChakraProvider>
  );
}
export default MyApp;
