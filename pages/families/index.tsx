import { Box, Center, Flex, Text } from "@chakra-ui/react";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React from "react";
import FamilyListCard from "../../components/familyListCard";
import AddFamilyModal from "../../components/modals/addFamilyModal";
import JoinFamilyModal from "../../components/modals/joinFamilyModal";
import { useMeQuery, useMyFamiliesQuery } from "../../generated/graphql";
import withApollo from "../../lib/withApollo";

const Families: NextPage = () => {
  const { data: meData, loading: meDataLoading } = useMeQuery();
  const { data, loading, error } = useMyFamiliesQuery({ skip: !meData?.me });
  const router = useRouter();

  if (error) console.log(error);

  if (!meData?.me && meDataLoading) return <>načítání....</>;
  else if (!meData?.me && typeof window !== "undefined" && !meDataLoading) {
    router.push("/");
    return <></>;
  } else
    return (
      <>
        <Flex justifyContent="space-around" wrap="wrap">
          <Box w={["full", "50%", "auto"]} my={[2]} px={2}>
            <AddFamilyModal />
          </Box>
          <Box w={["full", "50%", "auto"]} my={[2]} px={2}>
            <JoinFamilyModal />
          </Box>
        </Flex>
        {loading && (
          <Center>
            <Text className="text-center"> načítání...</Text>
          </Center>
        )}
        {!error && !loading && data?.myFamilies && (
          <Flex wrap="wrap">
            {data.myFamilies.map((family) => (
              <Box w={["100%", "50%", "33%", "25%"]} key={family.id}>
                <FamilyListCard {...family} />
              </Box>
            ))}
          </Flex>
        )}
      </>
    );
};

export default withApollo(Families);
