import { Box, Flex, Heading, Text } from "@chakra-ui/layout";
import _ from "lodash";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React from "react";
import WishListCard from "../../components/wishListCard";
import CreateWishModal from "../../components/modals/createWishModal";
import LeaveFamilyModal from "../../components/modals/leaveFamilyModal";
import LinkButton from "../../components/shared/buttons/linkButton";
import { useFamilyQuery, useMeQuery } from "../../generated/graphql";
import withApollo from "../../lib/withApollo";
import { Button, IconButton, Tooltip } from "@chakra-ui/react";
import { BiRefresh } from "react-icons/bi";
import { FiRefreshCcw } from "react-icons/fi";
import { toast } from "react-toastify";

const Family: NextPage = () => {
  const router = useRouter();
  const familyId = typeof router.query.id === "string" ? router.query.id : null;
  const { data, loading, error, refetch } = useFamilyQuery({
    variables: { id: familyId as string },
    skip: familyId === null ? true : false,
  });
  const { data: meData, loading: meLoading } = useMeQuery();

  if (loading || !data?.family || !meData?.me || meLoading) return <></>;
  if (error) return <LinkButton href="/families">zpět na seznam</LinkButton>;

  const { family } = data;
  const { me } = meData;
  const wishesGrouped = _.groupBy(family.wishes, "user.id");
  return (
    <>
      <Flex>
        <LinkButton href="/families">zpět na seznam</LinkButton>
        <Box ml="auto" display="flex">
          <Tooltip label="aktualizovat" hasArrow placement="top">
            <IconButton
              icon={<FiRefreshCcw />}
              mr={2}
              aria-label="aktualizovat"
              colorScheme="teal"
              onClick={async () => {
                await refetch();
                toast.success("aktualizováno");
              }}
            />
          </Tooltip>

          <Box mr={2}>
            <CreateWishModal id={family.id} refetch={refetch} />
          </Box>
          <LeaveFamilyModal id={family.id} />
        </Box>
      </Flex>

      <Heading as="h3" textTransform="lowercase" textAlign="center">
        {family.name}
      </Heading>

      <Heading fontSize="md" mt={4}>
        moje přání
      </Heading>

      {!wishesGrouped[me.id] || !wishesGrouped[me.id].length ? (
        <Text textAlign="center" fontStyle="italic">
          zatím nemáte žádné přání
        </Text>
      ) : (
        <Flex flexWrap="wrap">
          {wishesGrouped[me.id]?.map((g) => (
            <Box w={["100%", "100%", "50%", "33%", "25%"]} key={g.id}>
              <WishListCard
                user={{ id: me.id }}
                gift={{ ...g }}
                familyId={family.id}
                refetch={refetch}
              />
            </Box>
          ))}
        </Flex>
      )}

      {family?.members
        ?.filter((m) => m.id !== meData.me!.id)
        .map((member) => (
          <div key={member.id}>
            <Heading
              fontSize="md"
              key={member.id}
              textTransform="lowercase"
              mt={4}
            >
              {member.username}
            </Heading>

            {!wishesGrouped[member.id] || !wishesGrouped[member.id].length ? (
              <Text textAlign="center" fontStyle="italic">
                uživatel nemá zatím žádné přání
              </Text>
            ) : (
              <Flex flexWrap="wrap">
                {wishesGrouped[member.id]?.map((g) => (
                  <Box w={["100%", "100%", "50%", "33%", "25%"]} key={g.id}>
                    <WishListCard
                      user={{ id: me.id }}
                      gift={{ ...g }}
                      familyId={family.id}
                      refetch={refetch}
                    />
                  </Box>
                ))}
              </Flex>
            )}
          </div>
        ))}
    </>
  );
};
export default withApollo(Family);
