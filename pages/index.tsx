import { getDataFromTree } from "@apollo/client/react/ssr";
import { Box, Center, Heading } from "@chakra-ui/layout";
import type { NextPage } from "next";
import Image from "next/image";
import React from "react";
import LinkButton from "../components/shared/buttons/linkButton";
import { useMeQuery } from "../generated/graphql";
import withApollo from "../lib/withApollo";
import indexPhoto from "../public/index.gif";

const Home: NextPage = () => {
  const { data: meData, loading } = useMeQuery();
  return (
    <>
      <Center display="flex" flexDirection="column">
        <Heading as="h3">však víš</Heading>

        <Box w="full" maxW="xl" my={4}>
          <Image src={indexPhoto} alt="index funny gif" layout="responsive" />
        </Box>

        <Heading as="h3">vo co go</Heading>
        {!loading && (
          <>
            {!meData?.me ? (
              <Box display="flex" justifyContent="center" mt={4}>
                <LinkButton href="/login">přihlásit</LinkButton>
                <LinkButton href="/registration" ml={2}>
                  registrovat
                </LinkButton>
              </Box>
            ) : (
              <Center my={4}>
                <LinkButton href="/families">přejít do aplikace</LinkButton>
              </Center>
            )}
          </>
        )}
      </Center>
    </>
  );
};

export default withApollo(Home, { getDataFromTree });
