import {
  Alert,
  AlertIcon,
  Box,
  Button,
  Center,
  Flex,
  Heading,
} from "@chakra-ui/react";
import { Form, Formik } from "formik";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { toast } from "react-toastify";
import * as Yup from "yup";
import LinkButton from "../components/shared/buttons/linkButton";
import CustomField from "../components/shared/inputs/CustomFiled";
import { FormError, useLoginMutation, useMeQuery } from "../generated/graphql";
import withApollo from "../lib/withApollo";

const LoginSchema = Yup.object().shape({
  username: Yup.string()
    .min(2, "příliš krátké!")
    .max(50, "příliš dlouhé!")
    .required("požadováno"),
  password: Yup.string()
    .min(2, "příliš krátké!")
    .max(255, "příliš dlouhé!")
    .required("požadováno"),
});

const Login: NextPage = () => {
  const router = useRouter();
  const [loginMutation, { loading }] = useLoginMutation();
  const [errorMessages, setErrorMessages] = useState<FormError[]>();
  const { data: meData } = useMeQuery();

  if (meData?.me && window) router.push("/");

  return (
    <Center display="flex" flexDirection="column">
      <Box w="full" maxW="md">
        <Heading as="h2" textAlign="center">
          přihlášení
        </Heading>
        <Formik
          initialValues={{
            username: "",
            password: "",
          }}
          validationSchema={LoginSchema}
          validateOnMount={true}
          onSubmit={async (values) => {
            try {
              const response = await loginMutation({
                variables: values,
                refetchQueries: ["me"],
              });
              if (response.data?.login.user) router.push("/");
              else if (response.data?.login.errors) {
                const { errors } = response.data.login;
                setErrorMessages(errors);
              }
            } catch (error: any) {
              toast.error("nastala nečekaná chyba");
            }
          }}
        >
          {({ isSubmitting, isValidating, isValid }) => (
            <Form>
              {errorMessages
                ?.filter((errMsg) => !errMsg.field)
                .map((errorMessage, index) => (
                  <Alert
                    status="error"
                    textTransform="lowercase"
                    mt={4}
                    key={`${errorMessage}-${index}`}
                  >
                    <AlertIcon />
                    {errorMessage.message}
                  </Alert>
                ))}

              <CustomField
                name="username"
                label="uživatelské jméno"
                customErrors={errorMessages?.filter(
                  (errMsg) => errMsg.field === "username"
                )}
                mt={4}
              />
              <CustomField
                name="password"
                type="password"
                label="heslo"
                customErrors={
                  errorMessages?.filter(
                    (errMsg) => errMsg.field === "password"
                  ) || undefined
                }
                mt={4}
              />

              <Flex mt={4} justifyContent="center">
                <Button
                  type="submit"
                  mr={2}
                  disabled={!isValid}
                  isLoading={isValidating || isSubmitting}
                >
                  {loading ? "načítání" : "přihlásit"}
                </Button>
                <LinkButton href="/registration" ml={2}>
                  ještě nemám účet
                </LinkButton>
              </Flex>
            </Form>
          )}
        </Formik>
      </Box>
    </Center>
  );
};

export default withApollo(Login);
